using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enigma : MonoBehaviour
{

    [SerializeField] private GameObject clue;
    [Header("ink JSON")]
    [SerializeField] private TextAsset inkJSON;

    private bool playerInrange;
    public bool talked;
    private void Awake(){
        talked = false;
        playerInrange = false;
        clue.SetActive(false);
    }

    private void Update(){

        if(playerInrange && !talked && !DialogueManager.GEtInstance().dialogueIsPlaying){
            clue.SetActive(true);
            if(Input.GetKeyDown(KeyCode.Space)){
                DialogueManager.GEtInstance().EnterDialogueMode(inkJSON);
                
            }
        }else{
            clue.SetActive(false);
        }
        
    }

    private void OnTriggerEnter2D(Collider2D other){
        if (other.CompareTag("Player") && !other.isTrigger){
            playerInrange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other){
        if (other.CompareTag("Player") && !other.isTrigger){
            
            playerInrange = false;
        }
    }


   
}
