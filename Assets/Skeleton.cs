using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skeleton : MonoBehaviour
{

    Animator animator;
    private Transform target;
    [SerializeField]
    private float speed;
    [SerializeField]
    private float maxRange;
    [SerializeField]
    private float minRange;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        
        target = FindObjectOfType<playerControler>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(target.position , transform.position) <= maxRange && Vector3.Distance(target.position, transform.position)>=minRange){
            followPlayer();
        }else{
            animator.SetBool("Moving", false);
        }
    }
    public void followPlayer(){
        animator.SetBool("Moving",true);
        animator.SetFloat("moveX", (target.position.x - transform.position.x));
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);

    }

    private void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "MyWeapon"){
            Vector2 difference = transform.position - other.transform.position;
            transform.position = new Vector2(transform.position.x + difference.x, transform.position.y + difference.y);
        }
    }
}
