using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private HealthManager healthman;
    public Slider healthBar;
    // Start is called before the first frame update
    void Start()
    {
        healthman = FindObjectOfType<HealthManager>();
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.maxValue = healthman.maxHealth;
        healthBar.value = healthman.currentHealth;
    }
}
