using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class playerControler : MonoBehaviour{
    public float collisionOffset = 0.05f;
    public ContactFilter2D movementFilter;
    public float moveSpeed = 1f;
    private Vector2 moveInput;
    DialogueManager dialogueManager;

    public PlaySound playSound;
    
    private Rigidbody2D rb;
    Animator animator;
    

    List<RaycastHit2D> castCollisions = new List<RaycastHit2D>();

    bool CanMove = true;
    // Start is called before the first frame update
    private void Start()
    {
        playSound = GetComponent<PlaySound>();
        rb = GetComponent<Rigidbody2D>();
        
        animator = GetComponent<Animator>();
    }
    
    // Update is called once per frame
    void FixedUpdate()
    {
        
        if(CanMove){
            if (moveInput != Vector2.zero){
                bool success = tryMove(moveInput);

                if(!success){
                    success = tryMove(new Vector2(moveInput.x, 0));

                    if(!success){
                        success = tryMove(new Vector2(0, moveInput.y));
                    }
                }
                animator.SetBool("isMoving", success);
            }else{
                animator.SetBool("isMoving", false);
            }
            
        }
    }

    private bool tryMove(Vector2 direction){
        int count = rb.Cast(
                direction,
                movementFilter,
                castCollisions,
                moveSpeed * Time.fixedDeltaTime + collisionOffset);
            if(count == 0){
                rb.MovePosition(rb.position + direction * moveSpeed * Time.fixedDeltaTime);
                return true;
            }else{
                return false;
            }
    }

    void OnMove(InputValue value)
    {
        moveInput = value.Get<Vector2>();
        if(moveInput != Vector2.zero){
            animator.SetFloat("Xinput", moveInput.x);
            animator.SetFloat("Yinput", moveInput.y);
        }
        
    }

    void OnFire(){
        animator.SetTrigger("SwordAttack");
        playSound.Play(0);
    }

    public void LockMovement(){
        CanMove = false;
    }

    public void UnlockMovement(){
        CanMove = true;
    }

    public void increasespeed(){
        moveSpeed += 0.2f;
    }

}
