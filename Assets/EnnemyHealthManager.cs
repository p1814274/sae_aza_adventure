using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyHealthManager : MonoBehaviour
{
    public int currentHealth;
    public int maxHealth;
    private bool flashActive;
    Animator animator;

    [SerializeField]
    private float flashlength = 0f;
    private float flashCounter = 0f;
    private SpriteRenderer EnnemySprite;
    bool isAlive = true; 
    bool isDead;
    // Start is called before the first frame update
    void Start()
    {
        EnnemySprite = GetComponent<SpriteRenderer>();
        animator.SetBool("isAlive", isAlive);
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (flashActive){
            if(flashCounter > flashlength*.99f){
                EnnemySprite.color = new Color(EnnemySprite.color.r, EnnemySprite.color.g, EnnemySprite.color.b, 0f);
            }
            else if(flashCounter > flashlength *.82f){
                EnnemySprite.color = new Color(EnnemySprite.color.r, EnnemySprite.color.g, EnnemySprite.color.b, 1f);
            }
            else if (flashCounter > flashlength *.66f){
                EnnemySprite.color = new Color(EnnemySprite.color.r, EnnemySprite.color.g, EnnemySprite.color.b, 0f);
            }
            else if (flashCounter > flashlength *.49f){
                EnnemySprite.color = new Color(EnnemySprite.color.r, EnnemySprite.color.g, EnnemySprite.color.b, 1f);
            }
            else if (flashCounter > flashlength *.33f){
                EnnemySprite.color = new Color(EnnemySprite.color.r, EnnemySprite.color.g, EnnemySprite.color.b, 0f);
            }
            else if (flashCounter > flashlength *.16f){
                EnnemySprite.color = new Color(EnnemySprite.color.r, EnnemySprite.color.g, EnnemySprite.color.b, 1f);
            }
            else if (flashCounter > 0f){
                EnnemySprite.color = new Color(EnnemySprite.color.r, EnnemySprite.color.g, EnnemySprite.color.b, 0f);
            }
            else{
                EnnemySprite.color = new Color(EnnemySprite.color.r, EnnemySprite.color.g, EnnemySprite.color.b, 1f);
                flashActive = false;
            }
            flashCounter -= Time.deltaTime;
            
        }
        
    }
    public void HurtEnnemy(int damageToGive){
        currentHealth -= damageToGive;
        flashActive = true;
        flashCounter = flashlength;
        if (currentHealth <=0){
            Destroy(gameObject);
        }
    }
}
