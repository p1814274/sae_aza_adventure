using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtEnnemy : MonoBehaviour
{

    public int damageToGive = 1;


    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame

    private void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Ennemi"){
            EnnemyHealthManager eHealthman;
            eHealthman = other.gameObject.GetComponent<EnnemyHealthManager>();
            eHealthman.HurtEnnemy(damageToGive);
        }
    }

    public void earndamage(){
        damageToGive += 1;
    }

}
