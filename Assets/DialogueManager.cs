using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Ink.Runtime;
using UnityEngine.EventSystems;

public class DialogueManager : MonoBehaviour
{
    [Header("Dialgue UI")]
    [SerializeField] private GameObject dialoguePanel;
    [SerializeField] private TextMeshProUGUI dialogueText;
    private Story currentStory;
    public bool dialogueIsPlaying { get; private set;}

    

    // Start is called before the first frame update
    private static DialogueManager instance;
    [Header("Choices UI")]
    [SerializeField] private GameObject[] choices;

    private TextMeshProUGUI[] ChoicesText;


    private void Awake(){
        if (instance != null){
            Debug.LogWarning("found more than one Dialogue Manager in the scene");
        }
        instance = this;
    }

    public static DialogueManager GEtInstance(){
        return instance;
    }

    private void Start(){
        dialogueIsPlaying = false;
        dialoguePanel.SetActive(false);

        ChoicesText = new TextMeshProUGUI[choices.Length];
        int index = 0;
        foreach (GameObject choice in choices){
            ChoicesText[index] = choice.GetComponentInChildren<TextMeshProUGUI>();
            index++;
        }  
        
    }

    private void Update(){
        if (!dialogueIsPlaying){
            return;
        }

        if(Input.GetKeyDown(KeyCode.Space)){
            ContinueStory();
        }
    }

    public void EnterDialogueMode(TextAsset inkJSON){
        currentStory = new Story(inkJSON.text);
        dialogueIsPlaying = true;
        dialoguePanel.SetActive(true);

        ContinueStory();
    }

    private IEnumerator ExitDialogueMode(){

        yield return new WaitForSeconds(0.2f);
        dialogueIsPlaying = false;
        dialoguePanel.SetActive(false);
        dialogueText.text = "";
    }

    private void ContinueStory(){
        if(currentStory.canContinue){
            dialogueText.text = currentStory.Continue();

            DispayChoices();
        }else{
            ExitDialogueMode();
        }
    }

    private void DispayChoices(){
        List<Choice> currentChoices = currentStory.currentChoices;

        if(currentChoices.Count > choices.Length){
            Debug.LogError("More Choices were given than the UI can support. Number of choices given:" + currentChoices.Count);
        }

        int index = 0;

        foreach(Choice choice in currentChoices){
            choices[index].gameObject.SetActive(true);
            ChoicesText[index].text = choice.text;
            index++;
        }
        for (int i = index; i < choices.Length; i++){
            choices[i].gameObject.SetActive(false);
        }
        StartCoroutine(selectFirstChoice());
    }

    private IEnumerator selectFirstChoice(){
        EventSystem.current.SetSelectedGameObject(null);
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(choices[0].gameObject);
    }

    public void MakeChoice(int choiceIndex){
        currentStory.ChooseChoiceIndex(choiceIndex);
    }
}
