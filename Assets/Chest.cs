using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chest : Interactable
{
    public bool isOpen;
    public GameObject dialogBox;
    public Text dialogText;
    public HurtEnnemy hurtEnnemyt;
    public HurtEnnemy hurtEnnemyd;
    public HurtEnnemy hurtEnnemyr;
    public HurtEnnemy hurtEnnemyl;
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && playerInrange)
        {
            if(!isOpen){
                animator.SetTrigger("ouvre");
                OpenChest();
            }else{
                ChestisOpen();
            }
        } 
    }

    public void OpenChest()
    {
        dialogBox.SetActive(true);
        dialogText.text = "Vous avez trouvé un fragment de l'algorithme originel d'Al-Khawârizmî. Vous sentez son pouvoir imprégner votre arme.";
        hurtEnnemyt.earndamage();
        hurtEnnemyd.earndamage();
        hurtEnnemyr.earndamage();
        hurtEnnemyl.earndamage();
        isOpen = true;
        animator.SetBool("isOpen" , true);
        context.Raise();

    }

    public void ChestisOpen(){
        dialogBox.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D other){
        if (other.CompareTag("Player") && !other.isTrigger && !isOpen){
            context.Raise();
            playerInrange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other){
        if (other.CompareTag("Player") && !other.isTrigger &&!isOpen){
            context.Raise();
            playerInrange = false;
        }
    }
    
    
}
