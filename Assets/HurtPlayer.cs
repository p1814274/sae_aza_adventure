using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HurtPlayer : MonoBehaviour
{
    private HealthManager healthman;
    private float waitToHurt = 2f;
    private bool IsTouching;
    [SerializeField]
    private int damageToGive = 10;
    public PlaySound playSound;

    // Start is called before the first frame update
    void Start()
    {
        playSound = GetComponent<PlaySound>();
        healthman = FindObjectOfType<HealthManager>();
    }

    // Update is called once per frame
    void Update()
    {
        /*if(realoading){
            waitToDeath -= Time.deltaTime;
            if(waitToDeath <= 0){
                SceneManager.LoadScene("SampleScene");
            }
        }*/

        if (IsTouching){
            waitToHurt -= Time.deltaTime;
            if (waitToHurt <=0){
                healthman.HurtPlayer(damageToGive);
                waitToHurt = 2f;
            }

        }
    }

    private void OnCollisionEnter2D(Collision2D other){
        if(other.collider.tag == "Player"){
            //Destroy(other.gameObject);
            //other.gameObject.SetActive(false);
            other.gameObject.GetComponent<HealthManager>().HurtPlayer(damageToGive);
            //realoading = true;
            playSound.Play(0);
            
        }

    }

    private void OnCollisionStay2D(Collision2D other){
        if(other.collider.tag == "Player"){
            IsTouching = true;
        }
        
    }

    private void OnCollisionExit2D(Collision2D other){
        if(other.collider.tag == "Player"){
            IsTouching = false;
            waitToHurt = 2f;
        }
    }
}
