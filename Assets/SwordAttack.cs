using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordAttack : MonoBehaviour
{
    public enum AttackDirectionside{
        left, right
    }

    public AttackDirectionside attackDirectionside;
    Collider2D SwordCollider;
    Vector2 RightAttackoffset;

    private void Start(){
        SwordCollider = GetComponent<Collider2D>();
        RightAttackoffset = transform.position;
    }

    public void Attack(){
        switch(attackDirectionside){
            case AttackDirectionside.left:
                AttackLeft();
                break;
            case AttackDirectionside.right:
                AttackRight();
                break;

        }
    }
    private void AttackRight(){
        SwordCollider.enabled = true;
        transform.position = RightAttackoffset;
    }

    private void AttackLeft(){
        SwordCollider.enabled = true;
        transform.position = new Vector3(RightAttackoffset.x * -1, RightAttackoffset.y);
    }

    public void StopAttack(){
        SwordCollider.enabled = false;
    }
}
